-- Find objects

-- Method 1
SELECT OBJECT_NAME(referencing_id),* FROM sys.sql_expression_dependencies  
WHERE referenced_id = OBJECT_ID('TABLE_NAME');   
GO

-- Method 2
SELECT
  Object_name(so.parent_object_id) Parent_Name,
  so .name [Objeto],
  so .type_desc [Tipo],
  so .create_date [Creado],
  sm.definition [Texto]
FROM sys .objects so
INNER JOIN sys. sql_modules sm ON so.object_id = sm.object_id
WHERE     CONVERT(VARCHAR(8000),sm.definition) LIKE '%TABLE_NAME%'


-- Find constraints 
SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
WHERE TABLE_NAME = 'TABLE_NAME';


-- Find references fks 
select 
    t.name as TableWithForeignKey, 
    fk.constraint_column_id as FK_PartNo, c.
    name as ForeignKeyColumn 
from 
    sys.foreign_key_columns as fk
inner join 
    sys.tables as t on fk.parent_object_id = t.object_id
inner join 
    sys.columns as c on fk.parent_object_id = c.object_id and fk.parent_column_id = c.column_id
where 
    fk.referenced_object_id = (select object_id 
                               from sys.tables 
                               where name = 'TABLE_NAME')
order by 
    TableWithForeignKey, FK_PartNo;
